import tensorflow as tf
import numpy as np

from GDSC3APP.src.models.residual import SEGroupedConvolutionalBlock, SEGroupedIdentityBlock
from GDSC3APP.src.models.normalization import L2Normalization

class Siamese(tf.keras.Model):
    """A generic siamese-type model, using one encoder and a Manhattan-distance based head with learned weights"""

    def __init__(self, encoder, activation='sigmoid'):
        super(Siamese, self).__init__()
        self.encoder = encoder
        self.dense = tf.keras.layers.Dense(1, activation=activation)

    def call(self, inputs):
        encoded_1 = self.encoder(inputs[0])
        encoded_2 = self.encoder(inputs[1])
        
        abs_diff = tf.math.abs(encoded_1 - encoded_2)
        
        return self.dense(abs_diff)
    
    def build(self, input_shape):
        dense_input_shape = self.encoder.compute_output_shape(input_shape)
        
        self.dense.build(dense_input_shape)
        super(Siamese, self).build((None, *input_shape))


def load_model(model_path):
    input_shape = (224, 224, 3)

    # Build the encoder part of the model
    encoder = tf.keras.Sequential([
        tf.keras.layers.Conv2D(64, (7, 7), strides=(2, 2), padding='same', 
                               input_shape=input_shape, kernel_initializer='he_normal'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.ReLU(),
        tf.keras.layers.Conv2D(64, (3, 3), strides=(2, 2), padding='same', kernel_initializer='he_normal'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.ReLU(),
        SEGroupedConvolutionalBlock((112, 112, 256), (3, 3), strides=(1, 1), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((112, 112, 256), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((112, 112, 256), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedConvolutionalBlock((224, 224, 512), (3, 3), strides=(2, 2), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((224, 224, 512), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((224, 224, 512), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((224, 224, 512), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedConvolutionalBlock((448, 448, 1024), (3, 3), strides=(2, 2), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedConvolutionalBlock((896, 896, 2048), (3, 3), strides=(2, 2), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((896, 896, 2048), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((896, 896, 2048), (3, 3), cardinality=8, reduction_ratio=16, end_activation='linear'),
        tf.keras.layers.GlobalAveragePooling2D(),
        L2Normalization()
    ])

    # Build the complete model
    siamese = Siamese(encoder, 'softplus')
    siamese.build((None, 224, 224, 3))
    siamese.load_weights(model_path + '/variables/variables')

    return siamese


def load_embeddings(embedding_path):
    embeddings = np.loadtxt(embedding_path)

    return embeddings


def load_file_names(file_names_path):
    files_names = np.loadtxt(file_names_path, dtype='str')

    return files_names


def compute_predictions(model, embeddings, img):

    # Compute the image embedding
    img = tf.reshape(img, (1, 224, 224, 3))
    img_embedded = model.encoder(img)

    # Compute distances to all other images by using the model head
    abs_diffs = tf.math.abs(embeddings - img_embedded)
    dists = model.dense(abs_diffs)

    return dists.numpy().ravel()


def load_image(img_path):
    img = tf.io.read_file(img_path)
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.resize(img, [224, 224])
    img = (img - 127.5) / 127.5

    return tf.reshape(img, (1, 224, 224, 3))
