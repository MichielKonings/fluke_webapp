Welcome,

Thank you for visiting this GitLab and being interested in the created application FlukeFlow for the Data Science Challenge II!

This web-application runs on a Python Flask backend which uses a built-in TensorFlow model to match your fluke image with the database of whale fluke images.

Future plans:
Run on AWS Elastic Beanstalk as a webservice
Improve the frontend for User Experience 