import os
import sys

# Flask
from flask import Flask, redirect, url_for, request, render_template, Response, jsonify, redirect
from werkzeug.utils import secure_filename
#from gevent.pywsgi import WSGIServer

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

from tensorflow.keras.applications.imagenet_utils import preprocess_input, decode_predictions
from tensorflow.keras.preprocessing import image

# Some utilites
import numpy as np
from util import base64_to_pil, create_whale_id_dict
from GDSC3APP.src.predictions import *

# Declare a flask app
app = Flask(__name__)

#Create whale_id dict
whale_dict = create_whale_id_dict('C:\\Users\\MKONINGS\\Documents\\cozone\\data_science_challenge2\\updated_set') #specify path to images train folders and test, this is used for matching whale id's

# Data path is where the embeddings + names are stored
data_path = "C:\\Users\\MKONINGS\\Documents\\cozone\\data_science_challenge2\\application\\keras-flask-deploy-webapp\\GDSC3APP\\data"

# Model path is where the model is stored
model_path = "C:\\Users\\MKONINGS\\Documents\\cozone\\data_science_challenge2\\application\\keras-flask-deploy-webapp\\GDSC3APP\\models"

# Load everything in; only need to do this once
model = load_model(model_path + "\\experiment_rylai")
embeddings = load_embeddings(data_path + "\\rylai_embeddings.gz")
file_names = load_file_names(data_path + "\\file_names.gz")

@app.route('/', methods=['GET'])
def index():
    # Main page
    return render_template('index.html')


@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'POST':
        # Get the image from post request and pre-process
        img = base64_to_pil(request.json)
        img = np.array(img)
        img = tf.convert_to_tensor(img)
        img = tf.image.resize(img, [224, 224])
        img = (img - 127.5) / 127.5
        img = tf.reshape(img, (1, 224, 224, 3))

        # Make prediction
        dists = compute_predictions(model, embeddings, img) ##

        # Process result for human beings     
        pred_indices = np.argsort(dists)[:5] ##
        results = file_names[pred_indices]
        
        # Serialize the result, you can add additional fields
        return jsonify(result=['/static/images/' + results[0], '/static/images/' + results[1], '/static/images/' + results[2], '/static/images/' + results[3], '/static/images/' + results[4]], whale_id = [whale_dict[results[0]], whale_dict[results[1]], whale_dict[results[2]], whale_dict[results[3]], whale_dict[results[4]]])
    return None


if __name__ == '__main__':
    app.run(debug=True)