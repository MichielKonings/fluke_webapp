from tensorflow.keras.layers import BatchNormalization, Conv2D, GlobalAveragePooling2D, Dense
from tensorflow.keras.layers import Activation, Layer
from tensorflow.keras import Sequential

import tensorflow as tf

class IdentityBlock(Layer):

    def __init__(self, filters, kernel_shape=(3, 3), end_activation='relu', **kwargs):
        super(IdentityBlock, self).__init__(**kwargs)

        filters1, filters2, filters3 = filters

        self.conv1 = Conv2D(filters1, (1, 1), padding='valid', kernel_initializer='he_normal')
        self.bn1 = BatchNormalization()
        self.relu1 = Activation('relu')

        self.conv2 = Conv2D(filters2, kernel_shape, padding='same', kernel_initializer='he_normal')
        self.bn2 = BatchNormalization()
        self.relu2 = Activation('relu')

        self.conv3 = Conv2D(filters3, (1, 1), padding='valid', kernel_initializer='he_normal')
        self.bn3 = BatchNormalization()
        self.end_activation = Activation(end_activation)

    def build(self, input_shape):
        self.conv1.build(input_shape)
        super(IdentityBlock, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        x_org = x

        x = self.relu1(self.bn1(self.conv1(x)))
        x = self.relu2(self.bn2(self.conv2(x)))
        x = self.bn3(self.conv3(x))

        return self.end_activation(x + x_org)

    def compute_output_shape(self, input_shape):
        return self.conv2.compute_output_shape(input_shape)


class ConvolutionalBlock(Layer):

    def __init__(self, filters, kernel_shape=(3, 3), strides=(2, 2), **kwargs):
        super(ConvolutionalBlock, self).__init__(**kwargs)

        filters1, filters2, filters3 = filters

        self.conv1 = Conv2D(filters1, (1, 1), strides=strides, kernel_initializer='he_normal')
        self.bn1 = BatchNormalization()
        self.relu1 = Activation('relu')

        self.conv2 = Conv2D(filters2, kernel_shape, padding='same', kernel_initializer='he_normal')
        self.bn2 = BatchNormalization()
        self.relu2 = Activation('relu')

        self.conv3 = Conv2D(filters3, (1, 1), padding='valid', kernel_initializer='he_normal')
        self.bn3 = BatchNormalization()

        self.conv_shortcut = Conv2D(filters3, (1, 1), strides=strides, kernel_initializer='he_normal')
        self.bn_shortcut = BatchNormalization()

        self.relu3 = Activation('relu')

    def build(self, input_shape):
        self.conv1.build(input_shape)
        super(ConvolutionalBlock, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        x_shortcut = x

        x = self.relu1(self.bn1(self.conv1(x)))
        x = self.relu2(self.bn2(self.conv2(x)))
        x = self.bn3(self.conv3(x))

        x_shortcut = self.bn_shortcut(self.conv_shortcut(x_shortcut))

        return self.relu3(x + x_shortcut)

    def compute_output_shape(self, input_shape):
        return self.conv2.compute_output_shape(input_shape)


class SEModule(Layer):
    def __init__(self, reduction_ratio, **kwargs):
        super(SEModule, self).__init__(**kwargs)

        self.reduction_ratio = reduction_ratio
        self.average_pooling = GlobalAveragePooling2D()
        
    def build(self, input_shape):
        channels = input_shape[-1]

        if len(input_shape) < 3 or len(input_shape) > 4:
            raise ValueError(f"Input shape length 3 or 4 expected, got {input_shape}.")

        if channels % self.reduction_ratio != 0:
            raise ValueError(f"The reduction ratio {self.reduction_ratio} has to divide the number of channels {channels}.")

        self.average_pooling.build(input_shape)
        self.dense1 = Dense(channels // self.reduction_ratio, activation='relu', kernel_initializer='he_normal')
        self.dense2 = Dense(channels, activation='sigmoid', kernel_initializer='glorot_uniform')

        super(SEModule, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        x_org = x

        x = self.average_pooling(x)
        x = self.dense1(x)
        x = self.dense2(x)

        x = tf.expand_dims(tf.expand_dims(x, 1), 1)

        return x_org * x

    def compute_output_shape(self, input_shape):
        return input_shape


class SEConvolutionalBlock(Layer):

    def __init__(self, filters, kernel_shape=(3, 3), strides=(2, 2), reduction_ratio=4, **kwargs):
        super(SEConvolutionalBlock, self).__init__(**kwargs)

        filters1, filters2, filters3 = filters

        self.conv1 = Conv2D(filters1, (1, 1), strides=strides, kernel_initializer='he_normal')
        self.bn1 = BatchNormalization()
        self.relu1 = Activation('relu')

        self.conv2 = Conv2D(filters2, kernel_shape, padding='same', kernel_initializer='he_normal')
        self.bn2 = BatchNormalization()
        self.relu2 = Activation('relu')

        self.conv3 = Conv2D(filters3, (1, 1), padding='valid', kernel_initializer='he_normal')
        self.bn3 = BatchNormalization()

        self.semodule = SEModule(reduction_ratio)

        self.conv_shortcut = Conv2D(filters3, (1, 1), strides=strides, kernel_initializer='he_normal')
        self.bn_shortcut = BatchNormalization()

        self.relu3 = Activation('relu')

    def build(self, input_shape):
        self.conv1.build(input_shape)
        super(SEConvolutionalBlock, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        x_shortcut = x

        x = self.relu1(self.bn1(self.conv1(x)))
        x = self.relu2(self.bn2(self.conv2(x)))
        x = self.bn3(self.conv3(x))

        x = self.semodule(x)

        x_shortcut = self.bn_shortcut(self.conv_shortcut(x_shortcut))

        return self.relu3(x + x_shortcut)

    def compute_output_shape(self, input_shape):
        return self.conv2.compute_output_shape(input_shape)


class SEIdentityBlock(Layer):

    def __init__(self, filters, kernel_shape=(3, 3), reduction_ratio=4, **kwargs):
        super(SEIdentityBlock, self).__init__(**kwargs)

        filters1, filters2, filters3 = filters

        self.conv1 = Conv2D(filters1, (1, 1), padding='valid', kernel_initializer='he_normal')
        self.bn1 = BatchNormalization()
        self.relu1 = Activation('relu')

        self.conv2 = Conv2D(filters2, kernel_shape, padding='same', kernel_initializer='he_normal')
        self.bn2 = BatchNormalization()
        self.relu2 = Activation('relu')

        self.conv3 = Conv2D(filters3, (1, 1), padding='valid', kernel_initializer='he_normal')
        self.bn3 = BatchNormalization()
        self.relu3 = Activation('relu')

        self.semodule = SEModule(reduction_ratio)

    def build(self, input_shape):
        self.conv1.build(input_shape)

        super(SEIdentityBlock, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        x_org = x

        x = self.relu1(self.bn1(self.conv1(x)))
        x = self.relu2(self.bn2(self.conv2(x)))
        x = self.bn3(self.conv3(x))

        x = self.semodule(x)

        return self.relu3(x + x_org)

    def compute_output_shape(self, input_shape):
        return self.conv2.compute_output_shape(input_shape)


class SEGroupedConvolutionalBlock(Layer):

    def __init__(self, filters, kernel_shape=(3, 3), strides=(2, 2), cardinality=32, reduction_ratio=4, \
                 memory_efficient=True, end_activation='relu', **kwargs):
        super(SEGroupedConvolutionalBlock, self).__init__(**kwargs)

        filters1, filters2, filters3 = filters

        for filtersn in filters[:2]:
            if filtersn % cardinality != 0:
                raise ValueError(f"The number of groups {cardinality} has to divide the total number of filters {filtersn}.")

        filters1 = filters1 // cardinality
        filters2 = filters2 // cardinality

        self.cardinality = cardinality
        self.memory_efficient = memory_efficient

        self.blocks = [
            Sequential([
                Conv2D(filters1, (1, 1), strides=strides, kernel_initializer='he_normal'),
                BatchNormalization(),
                Activation('relu'),
                Conv2D(filters2, kernel_shape, padding='same', kernel_initializer='he_normal'),
                BatchNormalization(),
                Activation('relu'),
                Conv2D(filters3, (1, 1), padding='valid', kernel_initializer='he_normal'),
            ])
            for i in range(cardinality)
        ]

        self.bn_agg = BatchNormalization()
        
        self.semodule = SEModule(reduction_ratio)

        self.conv_shortcut = Conv2D(filters3, (1, 1), strides=strides, kernel_initializer='he_normal')
        self.bn_shortcut = BatchNormalization()

        self.end_activation = Activation(end_activation)

    def build(self, input_shape):
        super(SEGroupedConvolutionalBlock, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        x_shortcut = x

        if self.memory_efficient:
            x_sum = self.blocks[0](x)
            for block in self.blocks[1:]:
                x_sum += block(x)

            x = self.bn_agg(x_sum)
        else:
            x = [block(x) for block in self.blocks]
            x = self.bn_agg(tf.reduce_sum(x, axis=0))

        x = self.semodule(x)

        x_shortcut = self.bn_shortcut(self.conv_shortcut(x_shortcut))

        return self.end_activation(x + x_shortcut)

    def compute_output_shape(self, input_shape):
        return self.conv_shortcut.compute_output_shape(input_shape)


class SEGroupedIdentityBlock(Layer):

    def __init__(self, filters, kernel_shape=(3, 3), cardinality=32, reduction_ratio=4, \
                 memory_efficient=True, end_activation='relu', **kwargs):
        super(SEGroupedIdentityBlock, self).__init__(**kwargs)

        filters1, filters2, filters3 = filters

        for filtersn in filters[:2]:
            if filtersn % cardinality != 0:
                raise ValueError(f"The number of groups {cardinality} has to divide the total number of filters {filtersn}.")

        filters1 = filters1 // cardinality
        filters2 = filters2 // cardinality

        self.cardinality = cardinality
        self.memory_efficient = memory_efficient

        self.blocks = [
            Sequential([
                Conv2D(filters1, (1, 1), padding='valid', kernel_initializer='he_normal'),
                BatchNormalization(),
                Activation('relu'),
                Conv2D(filters2, kernel_shape, padding='same', kernel_initializer='he_normal'),
                BatchNormalization(),
                Activation('relu'),
                Conv2D(filters3, (1, 1), padding='valid', kernel_initializer='he_normal'),
            ])
            for i in range(cardinality)
        ]

        self.bn_agg = BatchNormalization()
        self.end_activation = Activation(end_activation)

        self.semodule = SEModule(reduction_ratio)

    def build(self, input_shape):
        super(SEGroupedIdentityBlock, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        x_org = x

        if self.memory_efficient:
            x_sum = self.blocks[0](x)
            for block in self.blocks[1:]:
                x_sum += block(x)

            x = self.bn_agg(x_sum)
        else:
            x = [block(x) for block in self.blocks]
            x = self.bn_agg(tf.reduce_sum(x, axis=0))

        x = self.semodule(x)

        return self.end_activation(x + x_org)

    def compute_output_shape(self, input_shape):
        return input_shape


if __name__ == "__main__":
    class L2Normalization(tf.keras.layers.Layer):
    
        def __init__(self, epsilon=10**-12, **kwargs):
            super(L2Normalization, self).__init__(**kwargs)

            self.epsilon = epsilon

        def build(self, input_shape):
            super(L2Normalization, self).build(input_shape)  # Be sure to call this at the end

        def call(self, x):
            return tf.math.l2_normalize(x, axis=-1, epsilon=self.epsilon)

        def compute_output_shape(self, input_shape):
            return input_shape

    input_shape = (224, 224, 3)

    # encoder1 = tf.keras.Sequential([
    #     tf.keras.layers.Conv2D(64, (7, 7), strides=(2, 2), padding='same', 
    #                            input_shape=input_shape, kernel_initializer='he_normal'),
    #     tf.keras.layers.BatchNormalization(),
    #     tf.keras.layers.ReLU(),
    #     tf.keras.layers.Conv2D(64, (3, 3), strides=(2, 2), padding='same', kernel_initializer='he_normal'),
    #     tf.keras.layers.BatchNormalization(),
    #     tf.keras.layers.ReLU(),
    #     SEGroupedConvolutionalBlock((128, 128, 256), (3, 3), strides=(1, 1), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((128, 128, 256), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((128, 128, 256), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedConvolutionalBlock((256, 256, 512), (3, 3), strides=(2, 2), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((256, 256, 512), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((256, 256, 512), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((256, 256, 512), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedConvolutionalBlock((512, 512, 1024), (3, 3), strides=(2, 2), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((512, 512, 1024), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((512, 512, 1024), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((512, 512, 1024), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((512, 512, 1024), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((512, 512, 1024), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedConvolutionalBlock((1024, 1024, 2048), (3, 3), strides=(2, 2), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((1024, 1024, 2048), (3, 3), cardinality=16, reduction_ratio=16),
    #     SEGroupedIdentityBlock((1024, 1024, 2048), (3, 3), cardinality=16, reduction_ratio=16, end_activation='linear'),
    #     tf.keras.layers.GlobalAveragePooling2D(),
    #     L2Normalization()
    # ])

    encoder = tf.keras.Sequential([
        tf.keras.layers.Conv2D(64, (7, 7), strides=(2, 2), padding='same', 
                               input_shape=input_shape, kernel_initializer='he_normal'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.ReLU(),
        tf.keras.layers.Conv2D(64, (3, 3), strides=(2, 2), padding='same', kernel_initializer='he_normal'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.ReLU(),
        SEGroupedConvolutionalBlock((112, 112, 256), (3, 3), strides=(1, 1), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((112, 112, 256), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((112, 112, 256), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedConvolutionalBlock((224, 224, 512), (3, 3), strides=(2, 2), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((224, 224, 512), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((224, 224, 512), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((224, 224, 512), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedConvolutionalBlock((448, 448, 1024), (3, 3), strides=(2, 2), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((448, 448, 1024), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedConvolutionalBlock((896, 896, 2048), (3, 3), strides=(2, 2), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((896, 896, 2048), (3, 3), cardinality=8, reduction_ratio=16),
        SEGroupedIdentityBlock((896, 896, 2048), (3, 3), cardinality=8, reduction_ratio=16, end_activation='linear'),
        tf.keras.layers.GlobalAveragePooling2D(),
        L2Normalization()
    ])

    #encoder1 = SEGroupedConvolutionalBlock((128, 128, 128))

    test_img_batch = tf.random.uniform((32, 224, 224, 3), minval=-1, maxval=1, dtype='float32')

    test = tf.random.uniform((32, 4, 4, 128), minval=-1, maxval=1, dtype='float32')

    print(encoder(test_img_batch))

    print(encoder.summary())