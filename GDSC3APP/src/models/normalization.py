import tensorflow as tf

class L2Normalization(tf.keras.layers.Layer):
    
    def __init__(self, epsilon=10**-12, **kwargs):
        super(L2Normalization, self).__init__(**kwargs)

        self.epsilon = epsilon

    def build(self, input_shape):
        super(L2Normalization, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        return tf.math.l2_normalize(x, axis=-1, epsilon=self.epsilon)

    def compute_output_shape(self, input_shape):
        return input_shape