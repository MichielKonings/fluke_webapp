"""Utilities
"""
import re
import base64

import os
import glob
import numpy as np

from PIL import Image
from io import BytesIO


def base64_to_pil(img_base64):
    """
    Convert base64 image data to PIL image
    """
    image_data = re.sub('^data:image/.+;base64,', '', img_base64)
    pil_image = Image.open(BytesIO(base64.b64decode(image_data)))
    return pil_image


def np_to_base64(img_np):
    """
    Convert numpy image (RGB) to base64 string
    """
    img = Image.fromarray(img_np.astype('uint8'), 'RGB')
    buffered = BytesIO()
    img.save(buffered, format="PNG")
    return u"data:image/png;base64," + base64.b64encode(buffered.getvalue()).decode("ascii")


def create_whale_id_dict(directory):
    """
    Create dictionary which maps pictures to whale_id
    """
    whale_dict = {}
    for subdir, dirs, files in os.walk(directory + '\\train'):
        whale_id = (os.path.basename(subdir))
        for f in files:
            if not f in whale_dict:
                whale_dict[f] = whale_id
    for subdir, dirs, files in os.walk(directory + '\\test'):
        for f in files:
            if not f in whale_dict:
             whale_dict[f] = 'Not classified'
    return whale_dict   


