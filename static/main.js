//========================================================================
// Drag and drop image handling
//========================================================================

var fileDrag = document.getElementById("file-drag");
var fileSelect = document.getElementById("file-upload");

// Add event listeners
fileDrag.addEventListener("dragover", fileDragHover, false);
fileDrag.addEventListener("dragleave", fileDragHover, false);
fileDrag.addEventListener("drop", fileSelectHandler, false);
fileSelect.addEventListener("change", fileSelectHandler, false);

function fileDragHover(e) {
  // prevent default behaviour
  e.preventDefault();
  e.stopPropagation();

  fileDrag.className = e.type === "dragover" ? "upload-box dragover" : "upload-box";
}

function fileSelectHandler(e) {
  // handle file selecting
  var files = e.target.files || e.dataTransfer.files;
  fileDragHover(e);
  for (var i = 0, f; (f = files[i]); i++) {
    previewFile(f);
  }
}

//========================================================================
// Web page elements for functions to use
//========================================================================

var imagePreview = document.getElementById("image-preview");
var imageDisplay = document.getElementById("image-display");
var imageDisplay2 = document.getElementById("image-display-2");
var imageDisplay3 = document.getElementById("image-display-3");
var imageDisplay4 = document.getElementById("image-display-4");
var imageDisplay5 = document.getElementById("image-display-5");
var uploadCaption = document.getElementById("upload-caption");
var predResult = document.getElementById("pred-result");
var predResult2 = document.getElementById("pred-result-2");
var predResult3 = document.getElementById("pred-result-3");
var predResult4 = document.getElementById("pred-result-4");
var predResult5 = document.getElementById("pred-result-5");
var loader = document.getElementById("loader");

hide(imageDisplay)
hide(imageDisplay2)
hide(imageDisplay3)
hide(imageDisplay4)
hide(imageDisplay5)
hide(predResult);
hide(predResult2);
hide(predResult3);
hide(predResult4);
hide(predResult5);


//========================================================================
// Main button events
//========================================================================

function submitImage() {
  // action for the submit button
  console.log("submit");

  if (!imageDisplay.src || !imageDisplay.src.startsWith("data")) {
    window.alert("Please select an image before submit.");
    return;
  }

  loader.classList.remove("hidden");
  imageDisplay.classList.add("loading");

  // call the predict function of the backend
  predictImage(imageDisplay.src);
}

function clearImage() {
  // reset selected files
  fileSelect.value = "";

  // remove image sources and hide them
  imagePreview.src = "";
  imageDisplay.src = "";
  imageDisplay2.src = "";
  imageDisplay3.src = "";
  imageDisplay4.src = "";
  imageDisplay5.src = "";
  predResult.innerHTML = "";

  hide(imagePreview);
  hide(imageDisplay);
  hide(imageDisplay2);
  hide(imageDisplay3);
  hide(imageDisplay4);
  hide(imageDisplay5);
  hide(loader);
  hide(predResult);
  hide(predResult2);
  hide(predResult3);
  hide(predResult4);
  hide(predResult5);
  show(uploadCaption);

  imageDisplay.classList.remove("loading");
}

function previewFile(file) {
  // show the preview of the image
  console.log(file.name);
  var fileName = encodeURI(file.name);

  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onloadend = () => {
    imagePreview.src = URL.createObjectURL(file);

    show(imagePreview);
    hide(uploadCaption);

    // reset
    predResult.innerHTML = "";
    imageDisplay.classList.remove("loading");

    displayImage(reader.result, "image-display");
    displayImage(reader.result, "image-display-2");
    displayImage(reader.result, "image-display-3");
    displayImage(reader.result, "image-display-4");
    displayImage(reader.result, "image-display-5");
    hide(imageDisplay)
    hide(imageDisplay2)
    hide(imageDisplay3)
    hide(imageDisplay4)
    hide(imageDisplay5)
    hide(predResult);
    hide(predResult2);
    hide(predResult3);
    hide(predResult4);
    hide(predResult5);

  };
}

//========================================================================
// Helper functions
//========================================================================

function predictImage(image) {
  fetch("/predict", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(image)
  })
    .then(resp => {
      if (resp.ok)
        resp.json().then(data => {
          displayResult(data);
        });
    })
    .catch(err => {
      console.log("An error occured", err.message);
      window.alert("Oops! Something went wrong.");
    });
}

function displayImage(image, id) {
  // display image on given id <img> element
  let display = document.getElementById(id);
  display.src = image;
  show(display);
}

function displayResult(data) {
  // display the result
  // imageDisplay.classList.remove("loading");
  hide(loader);
  predResult.innerHTML = data.whale_id[0];
  predResult2.innerHTML = data.whale_id[1];
  predResult3.innerHTML = data.whale_id[2];
  predResult4.innerHTML = data.whale_id[3];
  predResult5.innerHTML = data.whale_id[4];
  displayImage(data.result[0], "image-display");
  displayImage(data.result[1], "image-display-2");
  displayImage(data.result[2], "image-display-3");
  displayImage(data.result[3], "image-display-4");
  displayImage(data.result[4], "image-display-5");

  show(predResult);
  show(predResult2);
  show(predResult3);
  show(predResult4);
  show(predResult5);
}

function hide(el) {
  // hide an element
  el.classList.add("hidden");
}

function show(el) {
  // show an element
  el.classList.remove("hidden");
}